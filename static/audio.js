$(document).ready(function(){
    new jPlayerPlaylist({
	jPlayer: "#music-player",
	cssSelectorAncestor: "#jp_container_1"
    }, [
	{
	    title:"11 красавіка",
	    free:true,
	    mp3:"media/music/mp3/00_april11.mp3",
	    oga:"media/music/ogg/00_april11.ogg"
	},
	{
	    title:"В грозу",
	    free:true,
	    mp3:"media/music/mp3/01_v_grozu.mp3",
	    oga:"media/music/ogg/01_v_grozu.ogg"
	},
	{
	    title:"Дождь стучит",
	    free:true,
	    mp3:"media/music/mp3/02_dozhd_stuchit.mp3",
	    oga:"media/music/ogg/02_dozhd_stuchit.ogg"
	},
	{
	    title:"Когда падал снег",
	    free:true,
	    mp3:"media/music/mp3/03_kogda_padal_sneg.mp3",
	    oga:"media/music/ogg/03_kogda_padal_sneg.ogg"
	},
	{
	    title:"Ладони",
	    free:true,
	    mp3:"media/music/mp3/04_ladoni.mp3",
	    oga:"media/music/ogg/04_ladoni.ogg"
	},
	{
	    title:"Мое небо",
	    free:true,
	    mp3:"media/music/mp3/05_moe_nebo.mp3",
	    oga:"media/music/ogg/05_moe_nebo.ogg"
	},
	{
	    title:"Музыкі",
	    free:true,
	    mp3:"media/music/mp3/06_muzyki.mp3",
	    oga:"media/music/ogg/06_muzyki.ogg"
	},
	{
	    title:"На 8 этаже",
	    free:true,
	    mp3:"media/music/mp3/07_na8_etazhe.mp3",
	    oga:"media/music/ogg/07_na8_etazhe.ogg"
	},
	{
	    title:"Нас осудят",
	    free:true,
	    mp3:"media/music/mp3/08_nas_osudiat.mp3",
	    oga:"media/music/ogg/08_nas_osudiat.ogg"
	},
	{
	    title:"Осеннее утро",
	    free:true,
	    mp3:"media/music/mp3/09_osennee_utro.mp3",
	    oga:"media/music/ogg/09_osennee_utro.ogg"
	},
	{
	    title:"Отчаянный",
	    free:true,
	    mp3:"media/music/mp3/10_otchayannyj.mp3",
	    oga:"media/music/ogg/10_otchayannyj.ogg"
	},
	{
	    title:"Пачакай мяне",
	    free:true,
	    mp3:"media/music/mp3/11_pachakay_miane.mp3",
	    oga:"media/music/ogg/11_pachakay_miane.ogg"
	},
	{
	    title:"Смех",
	    free:true,
	    mp3:"media/music/mp3/12_smeh.mp3",
	    oga:"media/music/ogg/12_smeh.ogg"
	},
	{
	    title:"Я ведаю",
	    free:true,
	    mp3:"media/music/mp3/13_ya_vedau.mp3",
	    oga:"media/music/ogg/13_ya_vedau.ogg"
	}
    ], {
	swfPath: "js",
	supplied: "oga, mp3",
	wmode: "window"
    });
});
